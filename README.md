﻿# Devops Project

This is our devops project. It consists of mutiple docker containers

 - Symfony API
 - React Client
 - Mercure Server
 - Postgres Database
 - Watchtower container
 - Varnish cache
# Overview
## Symfony

It is a symfony 5 api with complete crud capabilities cache tags and invalidation

## React Client
A react client app with full crud and async updates for items using mercure
	
## Mercure

A mercure Hub that helps pushing updates notifications from api to client

## Postgres Database

A database for data persistence

## Watchtower

A watchtower container that checks for updates on docker images and automatically updates containers

## Varnish

A cache server that reduces api load and decreases response time

# ENV
To properly run the api you need to provide some env app from an .env file or directly from docker compose
> A default .env file is provided [here](https://gitlab.com/devops-1-ec/project-devops/-/blob/master/.env.default)
> 
Either set the Env variables manualy or run the following command at the root of the repository and edit the .env as you wish

    cp .env.default .env

# Testing
To test the app run the following commands

 - For the api

        docker-compose -f docker-compose.test.yml run php php vendor/bin/simple-phpunit

 - For the client

        docker-compose -f docker-compose.test.yml up --abort-on-container-exit --exit-code-from e2e
 
# Development 
To run the project run the following commands at the root of the repository:

    docker-compose up -d

# Build

 - Build the Images

		docker-compose -f docker-compose-prod/docker-compose.build.yml pull --ignore-pull-failures
		docker-compose -f docker-compose-prod/docker-compose.build.yml build --pull

 - Push the image

		docker-compose -f docker-compose-prod/docker-compose.build.yml push

# Prod
To run the project run the following commands at the root of the repository:

    docker-compose -f docker-compose-prod/docker-compose.yml pull
    docker-compose -f docker-compose-prod/docker-compose.yml up -d
    
# CI Build

 - First add the following Env Variables to your CI
 

	    CLIENT_IMAGE=registry.example.com/project/client
	    NGINX_IMAGE=registry.example.com/project/nginx
	    PHP_IMAGE=registry.example.com/project/php
	    VARNISH_IMAGE=registry.example.com/project/varnish
	    REACT_APP_API_ENTRYPOINT=https://api.example.com

 - To run on gitlab :
	> Use this [.gitlab-ci.yml](https://gitlab.com/devops-1-ec/project-devops/-/blob/master/.gitlab-ci.yml) file to configure pipelines

 - To run on other CI
	 > Do it yourself i'm not your mom

