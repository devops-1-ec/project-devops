<?php

namespace App\Tests\Entity;

use App\Entity\Product;
use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{
    public function testName()
    {
        $comment = new Product();

        $comment->setName("Test00142");

        $this->assertEquals("Test00142", $comment->getName());
    }

    public function testPrice()
    {
        $comment = new Product();

        $comment->setPrice(1000);

        $this->assertEquals(1000, $comment->getPrice());
    }
}
